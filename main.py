import pannekake
import lonns_beregning
import geometri_kalkulator

mainOpt = "0"

while(mainOpt != "q"):
    mainOpt = input("Oppgi et tall! 1=Lønns-Beregning 2=Geometri Kalkulator 3=Pannekake Kalkulator q=Avslutt ")
    if(mainOpt == "1"):
        lonns_beregning.lonn()

    if(mainOpt == "2"):
        geometri_kalkulator.math()

    if(mainOpt == "3"):
        pannekake.pannekake()