from math import sqrt

def math():
    opt = "0"
    while(opt != "x"):
        opt = input("Oppgi et tall! 1=Kule Volum Regner 2=Firkant Areal Regner 3=Hypotenus Regner x=Avslutt: ")
        if(opt == "1"):
            input("Vennligs oppgi dine mål uten måle-enheter! Bytt ut x med din måle-enhet i resultatet. ENTER for å fortsette.")
            radius = int(input("Oppgi radiusen av kulen din: "))
            sphereV = (4/3) * 3.14159265359 * (radius * radius * radius)
            print("Volumet av kulen er", sphereV, "x^3")

        if(opt == "2"):
            input("Vennligs oppgi dine mål uten måle-enheter! Bytt ut x med din måle-enhet i resultatet. ENTER for å fortsette.")
            height = int(input("Oppgi høyden på firkanten: "))
            length = int(input("Oppgi lengden på firkanten: "))
            area = height * length
            print("Arealet av firkanten din er", area, "x^2")

        if(opt == "3"):
            input("Vennligs oppgi dine mål uten måle-enheter! Bytt ut x med din måle-enhet i resultatet. ENTER for å fortsette.")
            katet1 = int(input("Oppgi lengden på katet nummer 1: "))
            katet2 = int(input("Oppgi lengden på katet nummer 2: "))
            hypo = ((katet1 * katet1) + (katet2 * katet2))
            hypoFin = sqrt(hypo)
            print("Hypotenusen er", hypoFin, "x lang")